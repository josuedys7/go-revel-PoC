package models

import (
	"badgesapp/app/models/mongodb"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Badge struct {
	ID        bson.ObjectId `json:"id" bson:"_id"`
	Title     string        `json:"title" bson:"title"`
	Content   string        `json:"content" bson:"content"`
	Icon      string        `json:"icon" bson:"icon"`
	Type      string        `json:"type" bson:"type"`
	CreatedAt time.Time     `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time     `json:"updated_at" bson:"updated_at"`
}

func newBadgeCollection() *mongodb.Collection {
	return mongodb.NewCollectionSession("badges")
}

// AddBadge insert a new Badge into database and returns
// last inserted badge on success.
func AddBadge(m Badge) (badge Badge, err error) {
	c := newBadgeCollection()
	defer c.Close()
	m.ID = bson.NewObjectId()
	m.CreatedAt = time.Now()
	return m, c.Session.Insert(m)
}

// UpdateBadge update a Badge into database and returns
// last nil on success.
func (m Badge) UpdateBadge() error {
	c := newBadgeCollection()
	defer c.Close()

	err := c.Session.Update(bson.M{
		"_id": m.ID,
	}, bson.M{
		"$set": bson.M{
			"title": m.Title, "content": m.Content, "icon": m.Icon, "type": m.Type, "updatedAt": time.Now()},
	})
	return err
}

// DeleteBadge Delete Badge from database and returns
// last nil on success.
func (m Badge) DeleteBadge() error {
	c := newBadgeCollection()
	defer c.Close()

	err := c.Session.Remove(bson.M{"_id": m.ID})
	return err
}

// GetBadges Get all Badge from database and returns
// list of Badge on success
func GetBadges() ([]Badge, error) {
	var (
		badges []Badge
		err    error
	)

	c := newBadgeCollection()
	defer c.Close()

	err = c.Session.Find(nil).Sort("-createdAt").All(&badges)
	return badges, err
}

// GetBadge Get a Badge from database and returns
// a Badge on success
func GetBadge(id bson.ObjectId) (Badge, error) {
	var (
		badge Badge
		err   error
	)

	c := newBadgeCollection()
	defer c.Close()

	err = c.Session.Find(bson.M{"_id": id}).One(&badge)
	return badge, err
}
